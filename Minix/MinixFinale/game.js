let points = 1;
let displayPoints = points;
let upgrades = [];
let buttons = [];
let backdrop;
let startKo;
let prutKo;
let showPrutKo = false;
let store
let title
let tv
let yesButton, noButton;

let contractVisible = false;
let contractText = "";

let earthStatusImages = []
let currentEarthImage
let earthStatusIndex = 0

let endGame = false


function preload() {
  //Background, Cow, Store, Title, TV
  backdrop = loadImage('images/baggrund.png');
  startKo = loadImage('images/ko.png');
  prutKo = loadImage('images/prutKo.png');
  store = loadImage('images/store.png');
  title = loadImage('images/gameTitle.png');
  tv = loadImage('images/tv.png');
  contractImage = loadImage('images/contract.png')


  //Store investment items
  airplane = loadImage('images/airplane.png');
  farm = loadImage('images/farm.png');
  bitcoin = loadImage('images/bitcoin.png');
  truck = loadImage('images/lastbil.png');
  car = loadImage('images/carpixel.png');
  oilRig = loadImage('images/oilRig.png');
  cruiseShip = loadImage('images/cruiseshipcopy.png');
  clothingFactory = loadImage('images/toejproduktion.png');
  containerShip = loadImage('images/containership.png');
  cow = loadImage('images/iconCow.png');
  chemicalPlant = loadImage('images/chemicalfactory.png');
  coalPowerPlant = loadImage('images/powerplant.png')

  //Burning Earth and endgame
  earthStatusImages.push(loadImage('images/jorden.png'))
  earthStatusImages.push(loadImage('images/jordenBraender1.png'))
  earthStatusImages.push(loadImage('images/jordenBraender2.png'))
  earthStatusImages.push(loadImage('images/jordenBraender3.png'))
  earthStatusImages.push(loadImage('images/jordenBraender4.png'))
  earthStatusImages.push(loadImage('images/jordenBraender5.png'))
  space = loadImage('images/space.png')


  //Background Music
  sound = loadSound('assets/cow.mp3');

  //Fonts
  gameFont = loadFont('assets/dayDream.ttf')
  contractFont = loadFont('assets/Motorola.ttf')
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(backdrop);
  textFont(gameFont)
  textAlign(CENTER)
  fill(16, 59, 27)
  createCowButton();
  loadPointsFormLocalStorage()


  sound.loop();

  currentEarthImage = earthStatusImages[earthStatusIndex]

  // new classes
  // upgradeName, startCost, pointProduction, milestone, co2Production, Image name
  cow = new Upgrade('Cow', 15, 1, 0, 3, cow);
  car = new Upgrade("Car", 100, 3, 25, 5, car);
  truck = new Upgrade("Truck", 250, 10, 95, 250, truck);
  plane = new Upgrade("Airplane", 1000, 50, 500, 135, airplane);
  farm = new Upgrade("Farm", 2000, 100, 1500, 300, farm);
  chemicalPlant = new Upgrade("Chemical Plant", 5000, 300, 3000, 15000, chemicalPlant);
  cruiseShip = new Upgrade("Cruise Ship", 15000, 600, 8000, 40000, cruiseShip);
  containerShip = new Upgrade("Container Ship", 40000, 1200, 10000, 88000, containerShip);
  coalPowerPlant = new Upgrade("Coal Power Plant", 125000, 6000, 50000, 380000, coalPowerPlant);
  oilRig = new Upgrade("Oil Rig", 400000, 12000, 50000, 3860000, oilRig);
  clothingFactory = new Upgrade("Clothing Factory", 1800000, 20000, 75000, 6000000, clothingFactory);
  bitcoin = new Upgrade("Bitcoin", 10000000, 100000, 250000, 65000000, bitcoin);

  // make the buttons once
  displayArrayOfButtons();
  contractButtons();
}


function displayArrayOfButtons() {
  for (let button of buttons) {
    button.upgradeButtonGraphics();
  }
}

// main button to start the game. becomes useless really fast (maybe change?)
function createCowButton() {
  image(startKo, windowWidth / 2, 200, 100, 100);
}

// gives the main button a function (haha)
function cowClicked() {
  points++;
}

// buttons
function contractButtons() {
  // Yes button
  yesButton = createButton('______');
  yesButton.position(windowWidth / 2 - 100, windowHeight / 2 + 110);
  yesButton.class('styled-yes-button');
  yesButton.hide();

  // No button
  noButton = createButton('x');
  noButton.position(windowWidth / 2 - 195, windowHeight / 4.4);
  noButton.class('styled-no-button');
  noButton.mousePressed(hideContractButtons);
  noButton.hide();
}

function showContractButtons() {
  yesButton.show();
  noButton.show();
}

function hideContractButtons() {
  yesButton.hide();
  noButton.hide();
}

// goes through all upgrades gives points
function givePoints() {
  for (let upgrade of upgrades) {
    upgrade.upgradePointPayout();
  }
}

// calculates the total amount of points the player is producing
function calculateTotalDPS() {
  let totalDPS = 0;
  for (let upgrade of upgrades) {
    totalDPS += upgrade.pointProduction * upgrade.upgradeAmount;
  }
  return totalDPS;
}

function calculateTotalCPS() {
  let totalCPS = 0;
  for (let upgrade of upgrades) {
    totalCPS += (upgrade.co2Production * upgrade.upgradeAmount)
  }

  return floor(totalCPS);
}
// 4.000, 50.000, 500.000, 10.000.000, 300,000,000, 500.000.000
function updateEarthImage(){
  let totalCPS = calculateTotalCPS()
  currentEarthImage = earthStatusImages[earthStatusIndex]
  if(totalCPS >= 4000 && earthStatusIndex == 0 ){earthStatusIndex = 1}
  if(totalCPS >= 500000 && earthStatusIndex == 1 ){earthStatusIndex = 2}
  if(totalCPS >= 5000000 && earthStatusIndex == 2){earthStatusIndex = 3}
  if(totalCPS >= 50000000 && earthStatusIndex == 3){earthStatusIndex = 4}
  if(totalCPS >= 300000000 && earthStatusIndex == 4){
    earthStatusIndex = 5
    endGame = true
  }
}

function gameOver(){
  image(space, width/2, height/2, 2056, 1440)
  earthStatusIndex = 5
  image(currentEarthImage, windowWidth/2, windowHeight/2-20, 600, 600)
  fill(255,0,0)
  textSize(50)
  text("Your greed destroyed the Earth", windowWidth/2, windowHeight/2-300)
  textSize(40)
  text(`Hope it was worth it, there isnt a second earth...`, windowWidth/2, windowHeight/2+290)
}

function loadPointsFormLocalStorage(){
  let checkIfNullPoints = getItem("pointsSaved")
  if(checkIfNullPoints !== null){
    points = getItem("pointsSaved")
    displayPoints = points
  }
}

function draw() {
  windowResized()
  background(backdrop);

  givePoints();
  updateEarthImage()
  storeItem("pointsSaved", points)

  image(title, windowWidth - 550, 50, 500, 100);
  image(store, 15, 20, 700, 850);

  image(tv, windowWidth - 575, windowHeight - 700, 550, 550);
  image(currentEarthImage, windowWidth - 472, windowHeight - 530, 250, 250)


  // Display the correct cow image based on showPrutKo flag
  if (showPrutKo) {
    image(prutKo, windowWidth / 2 - 155, windowHeight / 2 - 100, 350, 200);
  } else {
    image(startKo, windowWidth / 2 - 165, windowHeight / 2 - 100, 300, 200);
  }

  // displays the info for each upgrade
  for (let upgrade of upgrades) {
    upgrade.checkIfMilestoneReached();
    upgrade.upgradeButtonGraphics();
    upgrade.upgradeInfoDisplay();
    upgrade.gameOverHideDisplay()
  }

  // displays the total points and dps
  push()
  textSize(40)
  text(`${displayPoints} $`, windowWidth / 2, windowHeight / 2 - 180);
  textSize(22)
  let totalDPS = calculateTotalDPS();
  text(`${totalDPS} $ 
  per second`, windowWidth / 2, windowHeight / 2 + 150);
  let totalCPS = calculateTotalCPS()
  text(`You produce 
  ${totalCPS} 
  tons CO2 per year `, windowWidth - 300, windowHeight - 200);
  pop()

  // contract visuals
  if (contractVisible) {
    push()
    fill(255, 255, 255, 200);
    image(contractImage, width / 2, height / 2, 550, 550)
    fill(0)

    textFont(contractFont)
    textSize(22);
    text(contractText, width / 2 - 150, height / 2 - 120, 300, 500);
    text("sign contract?", width / 2, height / 2 + 100)
    textAlign(CENTER, CENTER)
    pop()
  }

  if(endGame){gameOver()}
}

function mousePressed() {
  let cowButtonWidth = 500;
  let cowButtonHeight = 500;
  let cowButtonX = (windowWidth - cowButtonWidth) / 2;
  let cowButtonY = 300;

  if (mouseX > cowButtonX && mouseX < cowButtonX + cowButtonWidth && mouseY > cowButtonY && mouseY < cowButtonY + cowButtonHeight) {
    // Call the function for cow clicked
    cowClicked();
    showPrutKo = true;

  }
}

function mouseReleased() {
  showPrutKo = false;
}