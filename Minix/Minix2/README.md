> # MiniX2


[Click to view my Minix1](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix2)

[Click to view my code](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix2/sketch.js)

![Image1](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix2/Sk%C3%A6rmbillede_2024-03-10_kl._19.01.08.png?ref_type=heads)

![Image2](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix2/Sk%C3%A6rmbillede_2024-03-10_kl._19.01.13.png?ref_type=heads)

## **Describe your program and what you have used and learnt**

The program is a simple interactive sketch. It draws a classic yellow smiley on a canvas, with eyes that follow the mouse cursor and the ability to toggle a light on and off by clicking on the face. 

**Variables**:
- Variables are containers for storing data values. In this program, variables like `eyeSize`, `eyeDistance`, `shadowDiameter`, `centerX`, `centerY`, and `isLightOn` are used to store values such as sizes, distances, and states that are used throughout the program.

**Creating the smiley**:
- The program draws circles representing the face, eyes, pupils, mouth and a shadow, by using the function `circle()`

**Conditional Statements**:
- Conditional statements like `if` and `else` are used to check conditions and execute different blocks of code based on those conditions. In this program, conditional statements are used to check whether the light is on and execute the appropriate drawing functions accordingly. 

**User Interaction**:
- User interaction is implemented using the `mousePressed()` function, which is called whenever the mouse button is pressed. In this program, clicking on the face toggles the state of the `isLightOn` variable, thereby turning the light on or off.
- Furthermore user interaction is used as the program tracks the mouse cursor's position using the `mouseX` and `mouseY` variables. The `createPupils()` function calculates the position of the pupils based on the mouse position, giving the illusion that the eyes are following the cursor.

**Text Display**:
- Text is displayed on the canvas using the `text()` function. 

## **How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**

In crafting my emoji, I opted to depict a modern rendition of the iconic smiley face, renowned for its minimalist circular design and yellow color. The choice of a yellow complexion aims to steer clear of any racial implications, opting instead for a color commonly associated with neutrality in emoji culture. However, this attempt at neutrality inadvertently risks erasing the racial and ethnic diversity among users, counteracting the intended inclusivity.

Likewise, the decision to eschew specific facial features like facial hair or freckles seeks to maintain gender neutrality. Yet, it's a subject of debate whether the classic smiley face truly achieves this, often being perceived as a representation of a bald white man. While this portrayal of a "generic" human face is ubiquitous, it falls short in many aspects of inclusivity. Beyond ethnic diversity, it's also crucial to consider representing other aspects, such as various disabilities for true inclusivity. So, where do we draw the line on inclusivity? Is the original yellow smiley face, beloved for decades, neutral enough, or do we need to push further? 

My decision to use the iconic smiley face as my base was rooted in its widespread recognition as a facial depiction. Its ubiquity made it a fitting choice for my purposes, aligning well with the style I aimed to achieve.

## **References**
https://p5js.org/reference/#/p5/

https://p5js.org/learn/interactivity.html

https://editor.p5js.org/AndyLucero/sketches/rkGGCadMM 

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-69