// Size of the eyes
let eyeSize = 40; 
// Distance between the eyes
let eyeDistance = 40; 
// Diameter of the shadow
let shadowDiameter = 200;
// Center coordinates of the canvas 
let centerX, centerY; 
// Tracking if the light is on
let isLightOn = true; 

function setup() {
  // Create canvas based on window size
  createCanvas(windowWidth, windowHeight); 
  // Calculate center X coordinate
  centerX = width / 2; 
  // Calculate center Y coordinate
  centerY = height / 2; 
}

function draw() {
  // Check if the light is on
  if (isLightOn) { 
    // Turn on light
    lightOn(); 
  } else {
    // Turn off light
    lightOff(); 
  }
}

function lightOn() {
  //Backgroundcolor
  background(166, 47, 71);
  
  //Shadow
  fill(96, 24, 39); // Shadow color
  circle(centerX - 10, centerY + 10, shadowDiameter, shadowDiameter); // Draw shadow

  //Face
  fill('#FFD700'); // Yellow face color
  noStroke(); // No stroke for face
  circle(centerX, centerY, 200, 200); // Draw face
  
  //Eyes
  fill(255); // White eyes
  circle(centerX - eyeDistance, centerY - 30, eyeSize, eyeSize); // Draw left eye
  circle(centerX + eyeDistance, centerY - 30, eyeSize, eyeSize); // Draw right eye
  
  //Pupils
  createPupils(centerX, centerY); // Create pupils for when the light is on
  
  //Mouth
  fill(0); // Color the mouth black
  circle(centerX, centerY + 40, 20, 10); // Create mouth
  
  //Text
  fill(255); // White text color
  textSize(20); // Set text size
  textAlign(CENTER, CENTER); // Center text horizontally and vertically
  text("Don't you dare touch me", centerX, centerY + 150); // Display text
}

function lightOff() {
  background(0); // Set black background
  
  fill(255); // White eyes
  circle(centerX - eyeDistance, centerY - 30, eyeSize, eyeSize); // Draw left eye
  circle(centerX + eyeDistance, centerY - 30, eyeSize, eyeSize); // Draw right eye
  
  createPupils(centerX, centerY); // Create pupils for when the light is off
}

function createPupils(x, y) {
  let pupilSize = 15; // Size of the pupils
  
  // Calculate position of left pupil
  let leftPupilX = map(mouseX, 0, width, x - eyeDistance - 10, x - eyeDistance + 10);
  let leftPupilY = map(mouseY, 0, height, y - 40, y - 20);
  
  // Calculate position of right pupil
  let rightPupilX = map(mouseX, 0, width, x + eyeDistance - 10, x + eyeDistance + 10);
  let rightPupilY = map(mouseY, 0, height, y - 40, y - 20);
  
  // Draw left and right pupils
  fill('black'); // Black pupils
  circle(leftPupilX, leftPupilY, pupilSize, pupilSize); // Left pupil
  circle(rightPupilX, rightPupilY, pupilSize, pupilSize); // Right pupil
}


function mousePressed() {
  let distance = dist(mouseX, mouseY, centerX, centerY); // Calculate distance between mouse and face center
  if (distance < 100) { // Check if mouse is inside the face (button)
    isLightOn = !isLightOn; // Toggle light state
  }
}
