// Declare x and y coordinates of the center of the canvas
let centerX, centerY;

// Tracking if the face is visible
let isFaceVisible = true; 

// Declare face and eyes variables 
let circleFaceDiameter = 200;
let eyeDiameter = 40; 
let eyeDistanceFromCenter = 40; 
let pupilDiameter = 30; // Diameter of the pupils

function setup() {
  createCanvas(windowWidth, windowHeight); 

 // Initialize centerX and centerY with the center coordinates of the canvas
  centerX = width / 2;
  centerY = height / 2;
}

function draw() {
  //Checks if face is visivble, and runs the necessary function accordingly  
  if (isFaceVisible) { 
    faceVisible(); 
  } else {
    faceNotVisible(); 
  }
}

function faceVisible() {
  background(166, 47, 71);
  
  //Shadow
  fill(96, 24, 39); 
  circle(centerX - 10, centerY + 10, circleFaceDiameter);

  //Face
  fill(255, 197, 0); 
  noStroke(); 
  circle(centerX, centerY, circleFaceDiameter);
  
  //Eyes
  drawEyes(); 
  
  //Mouth
  fill(0); 
  circle(centerX, centerY + 40, 20, 10);
  
  //Text
  fill(255); 
  textSize(20);
  textAlign(CENTER, CENTER); 
  text("Don't you dare touch me", centerX, centerY + 150);
}

function drawEyes() {
  fill(255); 
  circle(centerX - eyeDistanceFromCenter, centerY - 30, eyeDiameter); // Left eye
  circle(centerX + eyeDistanceFromCenter, centerY - 30, eyeDiameter); // Right eye
  
  //Pupils
  fill(0); 
  circle(centerX - eyeDistanceFromCenter + 5, centerY - 30, pupilDiameter); // Left pupil
  circle(centerX + eyeDistanceFromCenter + 5, centerY - 30, pupilDiameter); // Right pupil
}

function faceNotVisible() {
  background(0); 
  drawEyes()
}

function mousePressed() {
  let distance = dist(mouseX, mouseY, centerX, centerY); // Calculate distance between mouse and face center
  if (distance < circleFaceDiameter / 2) { // Check if mouse is inside the face
    isFaceVisible = !isFaceVisible; // Change booleanstatement to the opposite
  }
}
