let waitTextVisible = false;
let loadingText = "L O A D I N G . . ";
let amountOfTextRepeats = 100;
let textX = -10;

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(220);

  // Writes "Click?" if waitTextVisible = false
  if (!waitTextVisible) {
    fill(0); 
    textSize(16); 
    textAlign(CENTER, TOP); 
    text("Click?", width / 2, 10); 
  }

  // Checks if waitTextVisible = true, if yes display message
  if (waitTextVisible) {
    // Message
    background(0); 
    fill(195, 0, 0);
    textSize(28); 
    textAlign(CENTER, CENTER);

    // Forloop displaying wait text 
    for (let i = 0; i < 100; i++) {
      let x = random(width);
      let y = random(height);
      text("WAIT WAIT", x, y);
    }
  } else {
    // Display throbber LOADING text
    fill(0, 0, 255); 
    textSize(18);
    textAlign(CENTER, CENTER);
    // Repeat the "LOADING" text 
    text(loadingText.repeat(amountOfTextRepeats), textX, height / 2);
    
    // Continuous movement of loading text
    textX += 2; //change the value of textX by adding 2 

    // Resets textX's value (negative textWidth)
    if (textX > width) {
    textX = -textWidth(loadingText); // Start from the left edge of the canvas again
    }
  }
}

// Function to change the value of waitTextVisible 
function mousePressed() {
  waitTextVisible = !waitTextVisible; 
}
