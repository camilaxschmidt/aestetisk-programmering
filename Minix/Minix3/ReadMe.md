# Minix3

![Image1](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix3/Sk%C3%A6rmbillede_2024-03-17_kl._22.43.43.png?ref_type=heads)

![Image2](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix3/Sk%C3%A6rmbillede_2024-03-17_kl._22.43.55.png?ref_type=heads)

[RunMe](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix3)

[ReadMe](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix3/sketch.js?ref_type=heads)

## About My Design
This code depicts a basic interactive animation featuring a throbber. Initially, it displays a loading animation with "LOADING" moving horizontally across the screen. When the mouse is clicked, the animation switches to a waiting state, filling the screen with the message "WAIT WAIT" scattered randomly. Clicking the mouse again toggles back to the loading animation with the throbber. My code depicts impatience. When faced with a throbber, users can become impatient and repeatedly click the screen to check if the website is working.

In this code, I've used the looping mechanism, primarily driven by the draw() function, to create an animation. Within the draw() function, the canvas is continuously updated to reflect changes in the program's state. The loop operates continuously, ensuring smooth execution of the animation and seamless transitions between different states represented by the variable waitTextVisible.

When waitTextVisible is false, indicating the loading animation is active, the program dynamically updates the canvas by repeatedly drawing the "LOADING" text horizontally across it. This dynamic behavior is achieved by continuously updating the position of the loading text (textOffset) with each iteration of the loop. The loading animation repeats seamlessly as the draw() function iterates, enhancing the animation's fluidity and realism. Additionally, the animation loop responds to user interaction through the mousePressed() function. By clicking the mouse, users can toggle between the loading animation and the scattered "WAIT WAIT" message, adding an interactive element to the animation.

In the waiting phase, activated when waitTextVisible is true (when the mouse is pressed), the canvas fills with the message "WAIT WAIT" scattered randomly across its surface. This phase comprises the following steps:

The canvas background is set to black (background(0)) to provide contrast. Then, the text color is adjusted to a vibrant shade of red (fill(195, 0, 0)) to ensure clear visibility against the dark background.

Utilizing a loop that iterates 100 times, the program renders the "WAIT WAIT" text at various random positions across the canvas. To achieve randomness in the text's placement, the random() function generates random x and y coordinates within the canvas bounds. Additionally, small random offsets are added to both the x and y coordinates within the loop to introduce subtle variations in the text's positioning.

This process results in the canvas being populated with the words "WAIT WAIT" distributed haphazardly across its area. This scattered arrangement contributes to a visually dynamic and lively display, providing a unique visual experience during this phase of the animation.

## Reflection
**What Do You Want to Explore and/or Express?**

Impatience. Many of us have become impatient in regards to software. When faced with a throbber, and it is taking too long, we start to click the mouse to check if the website is still working. Therefore, I have created this hell of a waiting screen when clicking the mouse. It is meant to be a little scary, but that is the point. You have to wait. To be patient.

**Think About a Throbber That You Have Encountered in Digital Culture, e.g., for Streaming Video on YouTube or Loading the Latest Feeds on Facebook, or Waiting for a Payment Transaction, and Consider What a Throbber Communicates, and/or Hides? How Might We Characterize This Icon Differently?**

I think the actual throbber I have created is very simple. It is just text moving across the screen. Often times we see throbbers in a circular motion, whereas mine is more similar to a loading bar. A throbber or loading bar indicates that something is happening on the inside. Wheels are turning. But we cannot see it. Instead of putting a blank screen, the throbber was designed to depict just that.


### References
https://p5js.org/reference/#/p5/

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020