let microphone;
let fallingHarryHeadImg;
let microphoneImg;
let harryHeads = [];
let score = 0;
let gameOver = false;
let startScreen = true;
let startScreenText = "Falling with Harry";
let font;
let backgroundMusic;
let harryDadaSound;
let lastHarryHeadSpawn = 0;
let harryHeadSpawnInterval = 2000; // milliseconds

class Microphone {
  constructor() {
    this.size = 60;
    this.x = width / 2;
    this.y = height - 20;
  }

  constrainMicPositionX() {
    // Constrain the x position of the microphone within the canvas
    this.x = constrain(mouseX, 0, width);
  }

  display() {
    imageMode(CENTER);
    image(microphoneImg, this.x, this.y, this.size, this.size);
  }
}

class Harry {
  constructor() {
    // Randomly initialize the x position within the canvas width for Harry heads to spawn
    this.x = random(width);
    this.y = 0;
    this.size = 150;
    this.speed = random(1, 4);
  }

  update() {
    this.y += this.speed; // Move the Harry head downwards based on its speed
  }

  display() {
    imageMode(CENTER);
    image(fallingHarryHeadImg, this.x, this.y, this.size, this.size);
  }

  collision(microphone) {
    // Check if the Harry head collides with the microphone
    let distanceBetweenHarryAndMic = dist(this.x, this.y, microphone.x, microphone.y);
    return distanceBetweenHarryAndMic < this.size / 2 + microphone.size / 2; // Return true if they collide
  }
}

function preload() {
  fallingHarryHeadImg = loadImage('https://64.media.tumblr.com/f8e3877accdaff9dbf92f34b548667b1/8bb41f0068fc1183-83/s1280x1920/b1b4861c0bcfed6b74c2a707c7089f4f7a0e0bfb.png');
  microphoneImg = loadImage('https://64.media.tumblr.com/881a05680e258726fc4b6f88340996b0/1b2b478d1106136b-e2/s1280x1920/c8b921f22c65bc17a9acc836194f93d1037a64d9.png');

  backgroundMusic = loadSound('Falling.mp3');
  harryDadaSound = loadSound('Harrydada.mp3');

  font = loadFont('8bitFont.ttf');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  microphone = new Microphone();
}

function showStartScreen() {
  textSize(90);
  textAlign(CENTER, CENTER);
  fill(160, 82, 45);
  textFont(font);
  text(startScreenText, width / 2, height / 2 - 70);
  textSize(30);
  text("Catch Harry, he is falling!", width / 2, height / 2 - 10);
  text("Press Enter to Start", width / 2, height / 2 + 400);
}

function playGame() {
  background(236, 202, 154);

  // Display score
  textSize(40);
  fill(160, 82, 45);
  textFont(font);
  text("Score: " + score, 75, 30);

  // Check if it's time to spawn a new Harry head
  if (millis() - lastHarryHeadSpawn > harryHeadSpawnInterval) {
    harryHeads.push(new Harry());
    lastHarryHeadSpawn = millis();
  }

  microphone.constrainMicPositionX();
  microphone.display();

  // Loop through each Harry head in reverse order to avoid skipping elements when removing
  for (let i = harryHeads.length - 1; i >= 0; i--) {
    // Check if the current Harry head exists
    if (harryHeads[i]) {
      harryHeads[i].update();
      harryHeads[i].display();

      // Check if a Harry head hits the microphone and play sound
      if (harryHeads[i].collision(microphone)) {
        score++;
        harryHeads.splice(i, 1); // Remove the Harry head from the array
        harryDadaSound.setVolume(0.1);
        harryDadaSound.play();
      }

      // Check if a Harry head reaches the bottom of the canvas (if yes, Game Over)
      if (harryHeads[i] && harryHeads[i].y > height) {
        gameOver = true;
      }
    }
  }
}

function draw() {
  if (startScreen) {
    showStartScreen();
  } else if (!gameOver) {
    playGame();
  } else {
    // Game Over screen
    showGameOverScreen();
  }
}

function showGameOverScreen() {
  textSize(70);
  fill(160, 82, 45);
  textAlign(CENTER, CENTER);
  textFont(font);
  text("Game Over", width / 2, height / 2 - 30);
  textSize(30);
  text("Press Enter to Play Again", width / 2, height / 2 + 20);
  backgroundMusic.stop(); // Stop music when Game Over
}

// Function to start game when Enter is pressed
function keyPressed() {
  if (keyCode === ENTER) {
    startScreen = false;
    gameOver = false; // Restart the game when Enter key is pressed
    score = 0;
    harryHeads = []; // Reset array
    backgroundMusic.loop();
  }
}
