# Minix 6

![](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix6/Sk%C3%A6rmoptagelse_2024-04-15_kl._23.24.51.mov)

[RunMe](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix6)

[ReadMe](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix6/sketch.js)

## The code
**Describe how does/do your game/game objects work?**

In the game "Falling With Harry," players control a microphone to catch falling Harry heads. The main game objects are the Harry heads and the microphone.

Harry Heads: These are represented by instances of the Harry class. Each Harry head has attributes such as position (x and y), size (size), and speed. They fall from the top of the canvas towards the bottom at varying speeds. Players earn points by catching them with the microphone.

Microphone: This is represented by an instance of the Microphone class. It has attributes for size, position (x and y), and methods to update its position based on the player's mouse movement. The goal is to move the microphone to catch the falling Harry heads.

**Describe how you program the objects and their related attributes, and the methods in your game**

Objects such as Harry heads and the microphone are represented as classes (Harry and Microphone).
Each class has attributes to store the object's properties (e.g., position, size, speed).
These attributes are initialized when an object is created.
Programming Attributes:

Attributes are defined within each class and are used to store specific properties of the objects.
Examples of attributes include position (x and y coordinates), size (diameter or dimensions), and speed (for moving objects).
Programming Methods:

Methods are functions defined within each class that operate on the object's attributes.
For example, the update() method in both the Harry and Microphone classes updates the position of the object based on certain conditions (e.g., mouse movement for the microphone, falling speed for Harry heads).
The display() method in each class handles how the object is drawn on the canvas.
Additional methods, such as hits() in the Harry class, handle specific functionalities like collision detection.

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming (OOP) is a software design paradigm that emphasizes organizing code into manageable units known as objects. These objects encapsulate both data and the operations/methods that can be performed on that data. Through concepts like inheritance and encapsulation, OOP promotes code reuse, modularity, and maintainability. It's akin to assembling building blocks, where each block represents a specific piece of functionality, making the overall structure of the code more understandable and adaptable.

Abstraction, a fundamental principle of OOP, involves focusing on the essential aspects of a system while hiding unnecessary details. It allows developers to work with higher-level concepts without being burdened by implementation specifics. By abstracting away complexities, abstraction simplifies code comprehension and modification, making it easier to manage and extend over time. This approach fosters collaboration among developers by providing a clear and concise representation of the software's functionality.

Ultimately, OOP and abstraction together enable more efficient and error-resistant software development by promoting modularity, reusability, and clarity in code design and implementation.

## References
Soon Winnie & Cox, Geoff, “Auto-generator”, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 6.

[p5.js Refrences](https://p5js.org/reference/#/p5/)

[The Code Train - While and for loops](https://www.youtube.com/watch?v=cnRD9o6odjk&t=597s)