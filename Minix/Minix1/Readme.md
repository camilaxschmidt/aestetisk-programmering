[Click to view my Minix1](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix1)

[Click to view my code](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix1/sketch.js)

![Image1](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix1/Sk%C3%A6rmbillede_2024-03-03_kl._21.30.31.png?ref_type=heads)
![Image2](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix1/Sk%C3%A6rmbillede_2024-03-03_kl._21.30.23.png?ref_type=heads)


**What have you produced?**

Before undertaking this course, my exposure to coding was minimal. One of the initial graphics we coded involved circles, prompting me to seek improvement in my skills and explore interactive experiences centred around the circle as a shape. Recent lessons on mouse tracking provided valuable insights into incorporating user interaction into such experiences. This combined gave me the idea to make a code that generates an interactive canvas where circles appear as the user drags the mouse. Each drag action triggers the creation of circles with random sizes and colours, enriching the canvas with dynamic visual elements.

At the onset of my code, I established the canvas dimensions and background colour using the createCanvas() and background() functions, respectively. This set the stage for user engagement, with the canvas filling the entire window and the background colour set to a dark shade to accentuate the visual impact of the text. The employed text "Click and drag" is aligned and sized  to fit the in the centre of the canvas, prompting users to interact.
The crux of the interactive functionality lies within the mouseDragged() function. Each instance of mouse dragging triggers the generation of circles of varied colours and sizes, gradually populating the canvas. Furthermore, to enhance interactivity, I incorporated functionality to dynamically alter the background colour to a lighter colour upon mouse interaction, thereby heightening user engagement. The use of ´fill(random())´, I set random RGB colours for the circles and also variable diameter sizes with ´let diameter = random(50, 200);´, which adds a layer of visual diversity to the composition.
To maintain code clarity and prevent variable conflicts, I utilized the let keyword to declare variables such as diameter within the scope of the mouseDragged() function. This ensures that these variables are confined to the specific block of code where they are needed, reducing potential issues with other parts. The circle() function is instrumental in rendering each circle at the current mouse position, while noStroke() ensures a clean, outline-free appearance.

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

Fun yet challenging. As much as I loved exploring the endless possibilities in coding, I found it difficult to figure out what to create. The assignment was very open structure, and I could basically do whatever I wanted to - which is great yet terrifying. I tried a few different approaches to spark creativity, but ultimately searched for inspiration online - which worked! When the idea hit, it was somewhat smooth sailing. Of cause I had to figure out how to achieve my goal, but the idea was there. But I have realised the importance of understanding your code. At first it seemed simple; find a cool code online and then change a few bits and call it a day. That is fine, if you understand what has been made. Initially I had a completely different plan to my code, but was quickly met by my own confusion on the code examples i found online. I realised that I might have skipped a few steps and need to simplify things a little. But after all that, i have really enjoyed coding. There is still so much to learn, but I like the creative aspect of it - coding something and seeing it!

**How is the coding process different from, or similar to, reading and writing text?**

An obvious difference between coding and reading/writing is the creative aspect. It is a more visual process, in my opinion. You use a different way of being creative when writing text, where as in coding the creativity is somewhat more tangible. It feels more physical in a sense. In writing text, you can play with the syntax and make it your own by bending or ignoring the rules. That is not really possible in coding, where syntax is everything. Trying to 'create' your own style in code by e.g. purposefully writing words without consonants, as seen in text-writing, you will quickly stumble upon an array of problems. The code will not work if words are not spelled correctly, or even if certain letters are not capitalised. But nevertheless there is a similarity in the way of expressing yourself though a type of media. I do not really see much similarity between coding an reading, but reading and writing are essential tools in coding. I gain a lot from reading other peoples code, and interpret it by rewriting elements, to understand the code.

**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

Coding has slowly begun to become a tool that I can use to create or visualise ideas. I can see myself using it in my future to further explain visions and ideas in a way that can be easily understood. Though I know I will probably never become an expert in the subject, I feel that there is still so much for me to learn, which excites me. But reading Winnie Soon 's introduction to p5, has helped me immensely to understand this coding language.

**References**

https://p5js.org/reference/#/p5/

https://editor.p5js.org/sidchou/sketches/r1lg_uLYm

https://happycoding.io/tutorials/p5js/animation/random-lines

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-69
