function setup() {
  createCanvas(windowWidth, windowHeight);
  background(50);
  textAlign(CENTER, CENTER); // Set text alignment to center
  textSize(32); // Set text size
  fill(255); // Set text color to white
  text('Click and drag', width/2, height/2); // Display 'Click and drag' text at the center of the canvas
}



function mouseDragged() {
  // Set the background color to a light shade
  background(255, 10);
  // Set the fill color of the circle to a random RGB color
  fill(random(255), random(255), random(255));
  // Generate a random diameter between 50 and 200 pixels
  let diameter = random(50, 200);
  // Draw a circle at the mouse position with the random diameter
  circle(mouseX, mouseY, diameter);
  // Remove the outline of the circle
  noStroke();
}
