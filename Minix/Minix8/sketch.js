let currentQuote;
let backgroundImage;
let edYappingGif;
let edSound;
let font;

function preload() {
  backgroundImage = loadImage('divideBackground.png');
  edYappingGif = loadImage('edgif.gif');

  soundFormats('mp3');
  edSound = loadSound('ed.mp3');

  font = loadFont('font.ttf');
}

// Function to fetch and refresh the quote from the API
function refreshQuote() {
  loadJSON('https://api.kanye.rest', response => {
    currentQuote = response.quote; // Assign the fetched quote to the global variable 'currentQuote'
  });
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  refreshQuote(); // Fetch the first quote when the program starts

  let refreshButton = createButton('GET NEW <br> ED SHEERAN QUOTE');
  refreshButton.position((width - refreshButton.width) / 2, windowHeight / 2 - 20);
  refreshButton.mousePressed(refreshQuote); // Fetch a new quote when the button is pressed

  edSound.loop(); // Play the sound in a loop
}

function draw() {
  image(backgroundImage, 0, 0, windowWidth, windowHeight);
  image(edYappingGif, windowWidth / 1.75, 0, windowHeight, windowHeight);

  fill('yellow');
  textFont('font');
  textSize(windowWidth / 40);
  textAlign(CENTER, CENTER);

  if (currentQuote) {
    text(currentQuote, windowWidth / 12, windowHeight / 8, 600, 600); // Display the quote if available
  } else {
    text("AINT GON HAPPEN 🗣️‼️", windowWidth / 12, windowHeight / 8, 600, 600); // Display error message if no quote
  }
}
