let numberOfBubbles = 30;
let bubbles = [];


function setup() {
  createCanvas(windowWidth, windowHeight); 
  generateBubbles(); 
}

function draw() {
  background(204, 0, 127); 
  generateLava(); 
  fill(204, 0, 127); 
  noStroke()
  drawBubbles();
}

function generateBubbles() {
  for (let i = 0; i < numberOfBubbles; i++) {
    let x = random(width);
    let y = random(height); 
    let diameter = random(10, 110);
    let speedX = random(-2, 2); 
    let speedY = random(-2, 2); 
    bubbles.push({x, y, diameter, speedX, speedY}); 
  }
}

function generateLava() {
  // Maximum amplitude of the noise (y-axis)
  let maxLevelNoise = 1000; 
  // Controls the frequency of the noise pattern
  let noiseScale = 0.001; 

  for (let x = 0; x < windowWidth; x++) {
    // Scale x-coordinate for noise pattern
    let noiseScaledX = noiseScale * x;  
    // Calculate the noise value for the scaled x-coordinate
    let noiseScaledY = maxLevelNoise * noise(noiseScaledX);

    stroke(255, 153, 204);
    // line(x1,y1,x2,y2)
    line(x, 0, x, noiseScaledY);
  }
}


function drawBubbles() {
  for (let index = 0; index < bubbles.length; index++) { 
    let currentBubble = bubbles[index]; 

    ellipse(currentBubble.x, currentBubble.y, currentBubble.diameter);

    // Update bubble's position based on its speed
    currentBubble.x += currentBubble.speedX; 
    currentBubble.y += currentBubble.speedY;

    // Check if bubble hits the canvas edges and reverse its direction if necessary
    if (currentBubble.x < 0 || currentBubble.x > width) { //horizontal
      currentBubble.speedX *= -1; 
    }
    if (currentBubble.y < 0 || currentBubble.y > height) { //vertical
      currentBubble.speedY *= -1; 
    }
  }
}
