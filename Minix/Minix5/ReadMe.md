# Minix5 - Lavalamp

![](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix5/GIFMaker_me.gif)


[RunMe](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix5)


[ReadMe](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix5/sketch.js)

## The code
**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

In this generative program, the rules governing its behavior are fundamental yet dynamic.

Firstly, the program initiates by generating thirty bubbles, each possessing distinct attributes such as size, position, and velocity. These bubbles are scattered randomly across the canvas. 

Simultaneously, a dynamic backdrop reminiscent of a lava lamp emerges. This background is crafted using Perlin noise, gradually shifting and morphing over time. It resembles the fluid motion of a lava lamp, with colors blending and swirling in mesmerizing patterns.

As the program progresses over time, the bubbles navigate through this ever-changing environment. They move freely across the canvas, bouncing off its edges when they reach them. The bubbles' movements influence the flow of the background, creating ripples and disturbances in its pattern. Likewise, the shifting backdrop affects the trajectory of the bubbles, guiding their paths and interactions.

Together, these rules and interactions produce a continuously evolving canvas scene. 

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

In my code, bubbles are generated automatically following set rules, but they act independently as they move across the canvas. They follow initial parameters but have freedom within those boundaries.
This aligns with the chapter's focus on allowing emergent behavior by letting go of strict control. By using randomness in bubble creation, each bubble behaves uniquely, resulting in diverse and unpredictable visuals. The program illustrates the idea of embracing unpredictability in generative processes, echoing the chapter's theme.
## References
Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 5. 

[p5.js Refrences](https://p5js.org/reference/#/p5/)

[The Code Train - While and for loops](https://www.youtube.com/watch?v=cnRD9o6odjk&t=597s)