# Minix4 - Watching

![hello](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/raw/main/Minix/Minix4/ezgif-3-c1630b221f.gif?ref_type=heads)


[RunMe](https://camilaxschmidt.gitlab.io/aestetisk-programmering/Minix/Minix4)


[ReadMe](https://gitlab.com/camilaxschmidt/aestetisk-programmering/-/blob/main/Minix/Minix4/sketch.js?ref_type=heads)

## The code

This code utilizes the p5.js library to capture audio data from the user's microphone and visualize it on a canvas. Firstly, it initializes microphone capture using `p5.AudioIn()`, allowing real-time analysis of audio input. This enables the program to monitor the amplitude of the audio input as it changes over time.

Next, the amplitude of the audio input is continuously analyzed in the `draw()` function. When the amplitude exceeds a threshold of 0,01, a new "eye" object is created on the canvas. This threshold-based approach allows the program to respond to changes in the audio input dynamically.

The "eye" objects are represented by instances of the Eye class, which manages their position, size, and blinking behavior. The blinking animation of the eyes adds an interactive and engaging element to the visualization.

**Here is a rundown of how the blinking effect is created:**

1. **Initialization**: When an `Eye` object is created, its constructor initializes various properties including `isBlinking`, `lastBlinkTime`, and `blinkInterval`. 
   - `isBlinking`: This boolean property indicates whether the eye is currently blinking or not. It is initially set to `false`.
   - `lastBlinkTime`: This property stores the timestamp (in milliseconds) of when the eye last blinked.
   - `blinkInterval`: This property determines the interval between successive blinks. It is set to a random value between 500 and 3000 milliseconds.

2. **Update Method**: The `update()` method of the `Eye` class is responsible for updating the state of the eye, including its blinking behavior. It is called repeatedly in the `draw()` function.
   - Inside the `update()` method, there are conditions to check if it's time for the eye to blink:
     - If the eye is not already blinking (`!this.isBlinking`) and the elapsed time since the last blink exceeds the `blinkInterval`, the eye starts blinking. It sets `isBlinking` to `true` and updates `lastBlinkTime` to the current time.
     - If the eye is blinking and enough time has passed (100 milliseconds in this case), it stops blinking. It sets `isBlinking` back to `false`.

3. **Display Method**: The `display()` method of the `Eye` class is responsible for rendering the eye on the canvas. It adjusts the appearance of the eye based on whether it is blinking or not.
   - When the eye is not blinking (`!this.isBlinking`), the pupil is drawn normally.
   - When the eye is blinking, the code simulates closed eyelids. It calculates the percentage of eyelid closure based on the elapsed time since the last blink, and then draws closed eyelids with a decreasing height.

By managing the `isBlinking` flag and tracking the time since the last blink, the code controls when the eye should blink and how it appears during blinking, creating a simulated blinking effect.


## Capturing data

In today's digital age, our every move online leaves a trace — a digital footprint that's constantly being watched and analyzed. This process, known as data capture, encompasses every interaction we make on the internet. Tech giants and other entities meticulously collect this data, building a comprehensive profile of our preferences and behaviors. It's as if they're always watching and listening, gathering insights that sometimes surpass our own self-awareness.

It's crucial for consumers to stay aware of the data being collected about them. Developers need to prioritize creating software that securely collects and stores this data in a way that earns consumer trust. One of the biggest worries in the digital world is the possibility of our data being sold or used without our consent. By fostering transparency and ensuring ethical data practices, we can work towards building a safer and more trustworthy digital environment for everyone.

### References 

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020

p5.js Refrences: https://p5js.org/reference/#/p5/

Transmediale "Call for works, 2015" CAPTURE ALL: https://archive.transmediale.de/content/call-for-works-2015