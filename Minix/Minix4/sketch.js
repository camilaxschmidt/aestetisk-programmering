let microphoneAudioCapture; 
let eyes = []; 

function setup() {
  createCanvas(windowWidth, windowHeight); 
  startMic();
}

function draw() {
  background(0);

  if (microphoneAudioCapture) {
    let rms = microphoneAudioCapture.getLevel();

    if (rms > 0.01) {
      let x = random(width);
      let y = random(height);
      let diameter = random(20, 50);

      // Create a new Eye object with the random parameters and add it to the 'eyes' array
      eyes.push(new Eye(x, y, diameter));
    }
  }
  
  for (let eye of eyes) {
    eye.display();
  }
}

class Eye {
  constructor(x, y, diameter) {
    this.x = x;
    this.y = y; 
    this.diameter = diameter;
  }

  display() {
    // Draw eye
    fill(255); 
    ellipse(this.x, this.y, this.diameter, this.diameter / 2);

    // Draw pupil
    fill(0); 
    ellipse(this.x, this.y, this.diameter / 4, this.diameter / 4); 
  }
}

function startMic() {
  userStartAudio(); // Required for the audio context to start
  microphoneAudioCapture = new p5.AudioIn(); // Create a new AudioIn object for microphoneAudioCapturerophone input
  microphoneAudioCapture.start(); // Start capturing audio from the microphoneAudioCapturerophone
}
